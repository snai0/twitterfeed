﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterFeed.Models;

namespace TwitterFeed
{
    class Program
    {
        static void Main(string[] args)
        {
            var users = User.LoadUsers();

            foreach (var user in users)
            {
                //master list of tweets for each user, alos contains tweets of people subscribed too
                var tweets = new List<ITweet>();

                //current users tweets
                var userTweets = user.UserTweets().ToList();

                //add tweets to masterList
                userTweets.ForEach(tweet => tweets.Add(tweet));

                //tweets for each user followed
                foreach (var usersFollowed in user.UsersFollowed)
                {
                    //tweets of users, current user is subscribd too
                    var subscribedTweets = usersFollowed.UserTweets().ToList();

                    subscribedTweets.ForEach(tweet => tweets.Add(tweet)); //add tweets to master list
                }
                Console.WriteLine(user.Name);


                tweets = tweets.OrderBy(tweet => tweet.TweetNumber).ToList(); //Order tweets by position in file

                //print all tweets
                tweets.ForEach(tweet => Console.WriteLine("\t" + "@" + tweet.User + ": " + tweet.UserTweet));
                Console.WriteLine("\n");
            }
        }
    }
}