﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TwitterFeed.Models
{
    public class Tweet : ITweet
    {
        private static readonly string[] TweetFileLines;

        static Tweet()
        {
            try
            {
                TweetFileLines = File.ReadAllLines(@"AppData\tweet.txt");
            }
            catch (FileNotFoundException)
            {
                Console.Error.WriteLine("Fatal Erorr, tweets not found!");
                Console.ReadLine();
                Environment.Exit(0);
            }
        }

        public string User { get; }

        public string UserTweet { get; }

        public int TweetNumber { get; }

        public Tweet(int tweetNumber, string user, string userTweet)
        {
            TweetNumber = tweetNumber;
            User = user;
            UserTweet = userTweet;
        }

        public static IList<ITweet> LoadTweets()
        {
            var tweets = new List<ITweet>();
            int counter = 1;

            foreach (var _tweet in TweetFileLines)
            {
                var tweetLine = _tweet.Split('>');
                var tweet = new Tweet(counter, tweetLine[0], tweetLine[1]);
                tweets.Add(tweet);
                counter = counter + 1;
            }
            return tweets;
        }
    }
}