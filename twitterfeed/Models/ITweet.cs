﻿namespace TwitterFeed.Models
{
    public interface ITweet
    {
        string User { get; }
        string UserTweet { get; }
        int TweetNumber { get; }
    }
}