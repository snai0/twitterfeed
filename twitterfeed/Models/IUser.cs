using System.Collections.Generic;

namespace TwitterFeed.Models
{
    public interface IUser
    {
        string Name { get; }
        IList<User> UsersFollowed { get; }
        void AddUserSubscription(User user);
        void AddUser();
        bool UserExists();
        IEnumerable<ITweet> UserTweets();
    }
}