﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TwitterFeed.Models
{
    public class User : IUser
    {
        private static readonly List<User> Users;
        private static readonly string[] UserFileLines;
        private static readonly IList<ITweet> Tweets;
        
        static User()
        {
            try
            {
                UserFileLines = System.IO.File.ReadAllLines(@"AppData\user.txt");
            }
            catch (FileNotFoundException)
            {
                Console.Error.WriteLine("Fatal Erorr, tweets not found!");
                Console.ReadLine();
                Environment.Exit(0);
            }
            
            Users = new List<User>();
            Tweets = Tweet.LoadTweets();
        }


        public string Name { get; }

        public IList<User> UsersFollowed { get; }

        public User(string name)
        {
            Name = name;
            UsersFollowed = new List<User>();
        }


        public void AddUserSubscription(User user)
        {
            UsersFollowed.Add(user);
        }

        public static IList<User> LoadUsers()
        {
            foreach (var line in UserFileLines)
            {
                var tempLine = line.Split(' ').ToList();
                tempLine = tempLine.Where(p => p.Contains("follows") == false).ToList();

                var user = new User(tempLine[0]);
                user.AddUser();

                var subscriptions = tempLine[1].Split(',').ToList();
                subscriptions.ForEach(subscription => new User(subscription).AddUser());
                subscriptions.ForEach(subscription => user.AddUserSubscription(new User(subscription)));
            }

            return Users.OrderBy(u => u.Name).ToList();
        }

        private static User RetrieveUser(string name)
        {
            return Users.FirstOrDefault(user => user.Name == name);
        }

        public void AddUser()
        {
            if (!UserExists())
            {
                Users.Add(this);
            }
        }

        public bool UserExists()
        {
            return Users.Any(user => user.Name == Name);
        }
        
        public IEnumerable<ITweet> UserTweets()
        {
            var result = Tweets.Where(p => p.User.Contains(Name));
            return result;
        }
    }
}